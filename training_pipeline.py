import pickle
import requests
import json
import os
import time
from sys import platform
import pandas as pd
from tqdm import tqdm
import numpy as np
import pandas as pd
import sklearn.metrics as sbm
import matplotlib.pyplot as plt
# from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler, MinMaxScaler



p = []
datafiles = []
if platform == "linux" or platform == "linux2":
    filename = os.path.dirname(os.path.realpath(__file__))+"/config.json"
elif platform == "win32":
    filename = os.path.dirname(os.path.realpath(__file__))+"\\config.json"
with open(filename)as f:
    config = json.load(f)

def get_thingsboard_token(username,password,tb_ip):
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        }

    data = '{"username":"'+username+'", "password":"'+password+'"}'
    response = requests.post('http://'+tb_ip+'/api/auth/login', headers=headers, data=data)
    return response.json()['token']

def get_asset_id(tkn,tb_ip,asset_type):
    headers = {
        'Accept': 'application/json',
        'X-Authorization': 'Bearer '+tkn,
        }
    params = (
        ('pageSize', '30'),
        ('page', '0'),
        )
    response = requests.get('http://'+tb_ip+'/api/user/assets', headers=headers, params=params)
    res = response.json()['data']
    asset_id = [i for i in res if (i['type'] == asset_type)]
    return asset_id

def get_asset_relations(tkn,tb_ip,asset_id,device_type):
    headers = {
        'Accept': 'application/json',
        'X-Authorization': 'Bearer '+tkn,
        }
    params = (
        ('fromId', asset_id),
        ('fromType', 'ASSET'),
        )
    response = requests.get('http://'+tb_ip+'/api/relations/info', headers=headers, params=params)
    devices = [i['to']['id'] for i in response.json() if(any(device in i['toName'] for device in device_type.split(",")))]
    return devices

def get_device_parameters(tkn,tb_ip,devices,parameters):
    headers = {
        'Accept': 'application/json',
        'X-Authorization': 'Bearer '+tkn,
        }
    response = {}
    for i in devices:
        allparams = requests.get('http://'+tb_ip+'/api/plugins/telemetry/DEVICE/'+i+'/keys/timeseries', headers=headers).json()
        response[i] = ",".join([i for i in allparams if i in parameters])
    
    # z.append(response)
    return response

def get_flat(l):
    res = []
    for i in l:
        res.append(int(i["value"]))
    return sorted(res)

def get_cycles(starttime,endtime,tb_ip,j,token):
    headers = {
        'Accept': 'application/json',
        'X-Authorization': 'Bearer '+token,
        }
    params = (
        ('limit', '10000'),
        ('agg', 'NONE'),
        ('orderBy', 'DESC'),
        ('useStrictDataTypes', 'false'),
        ('keys', 'PON,POFF'),
        ('startTs', str(starttime)),
        ('endTs', str(endtime)),
        )
    params1 = (
        ('keys', 'Power_Status,DON,DOFF'),
    )

    response = requests.get('http://'+tb_ip+'/api/plugins/telemetry/ASSET/'+j+'/values/timeseries', headers=headers, params=params)
    resp = requests.get('http://'+tb_ip+'/api/plugins/telemetry/ASSET/'+j+'/values/attributes', headers=headers, params=params1)

    if("PON" not in list(response.json().keys()) or "POFF" not in list(response.json().keys())):
        return []
    res = {}
    for i in resp.json():
        res[i['key']] = i['value']
    # print(res["Power_Status"])
    PON = get_flat(response.json()["PON"])
    POFF = get_flat(response.json()["POFF"])
    # print("PON:",len(PON))
    # print("POFF:",len(POFF))

    # print("-----PON-------")
    # print(PON)
    # print(POFF)
    # print("------POFF------")

    cycles = []
    for i in PON:
        tem = []
        if(res['Power_Status'] == True):
            tem = sorted(list(set(POFF + PON +[(res['DON']*1000),round(time.time()*1000)])))
        else:
            tem = sorted(list(set(POFF + PON +[(res['DON']*1000),(res['DOFF']*1000)])))
        if(tem.index(i) != (len(tem)-1) and tem[tem.index(i)+1] not in PON):
            cycles.append((i,tem[tem.index(i)+1]))
    return cycles

def get_data(tkn,tb_ip,device_id,keys,start,end,aggregationinterval,aggregation):
    headers = {
        'Content-Type': 'application/json',
        'X-Authorization': 'Bearer '+tkn,
        }
    params = (
        ('keys', keys),
        ('startTs', str(start)),
        ('endTs', str(end)),
        ('interval', str(aggregationinterval)),
        ('limit', '1000000'),
        ('agg', aggregation.upper()),
        )
    response = requests.get('http://'+tb_ip+'/api/plugins/telemetry/DEVICE/'+device_id+'/values/timeseries', headers=headers, params=params)
    x = {}
    for j in list(response.json().keys()):
        x[j] = {i['ts']:i['value'] for i in response.json()[j]}
    # return response.json()
    return x
        
def fetch(start,end,aggregationinterval,aggregation):
    tkn = get_thingsboard_token(config['tanent_username'], config['tanent_password'], config['thingsboard_ip'])
    assets = get_asset_id(tkn, config['thingsboard_ip'], config['asset_type'])
    asset_ids = [i['id']['id'] for i in assets]
    asset_devices = {}
    asset_device_parameters = {}
    for i in asset_ids:
        asset_devices[i] = get_asset_relations(tkn, config['thingsboard_ip'], i,config['device_type'])
    for i in asset_ids:
        if len(asset_devices[i]) > 0:
            asset_device_parameters[i] = get_device_parameters(tkn, config['thingsboard_ip'], asset_devices[i],config['parameters'])
    
    cycles = {}
    for asset in asset_ids:
        cycles[asset] = get_cycles(start,end,config['thingsboard_ip'],asset,tkn)
    
    
    for i in list(asset_device_parameters.keys()):
        for cycle in tqdm(range(len(cycles[i]))):
            if (cycle == 0):
                for j in list(asset_device_parameters[i].keys()):
                    asset_device_parameters[i][j] = get_data(tkn, config['thingsboard_ip'],j, asset_device_parameters[i][j],cycles[i][cycle][0],cycles[i][cycle][1],aggregationinterval,aggregation)
            else:
                for j in list(asset_device_parameters[i].keys()):                   
                    x = get_data(tkn, config['thingsboard_ip'],j, asset_device_parameters[i][j],cycles[i][cycle][0],cycles[i][cycle][1],aggregationinterval,aggregation)
                    if(x != {}):
                        for k in list(asset_device_parameters[i][j].keys()):
                            try:
                                asset_device_parameters[i][j][k].update(x[k])
                            except:
                                pass

    x = asset_device_parameters
    print("got data")

    times = {}
    files = []
    for i in list(asset_device_parameters.keys()):
        for j in list(asset_device_parameters[i].keys()):
            times[str(j)] = []
            for k in list(asset_device_parameters[i][j].keys()):
                times[str(j)] = times[str(j)] + list(asset_device_parameters[i][j][k].keys())
                
    for i in list(asset_device_parameters.keys()):
        for j in list(asset_device_parameters[i].keys()):
            df = pd.DataFrame(times[j],columns=['timestamp'])
            for k in list(asset_device_parameters[i][j].keys()):
                df[k] = df['timestamp'].map(asset_device_parameters[i][j][k])
            # p.append(df)
            df.fillna(0)
            df.to_csv(str(i)+"_"+str(j)+".csv")
            files.append(str(i)+"_"+str(j)+".csv")
    return x, cycles, times, files

def get_min_max_ts(files, no_of_record):
    min_ts = []
    max_ts = []
    for i in files:
        min_ts.append(i['timestamp'].describe().to_dict()['min'])
        max_ts.append(i['timestamp'].describe().to_dict()['max'])
    min_ts, max_ts = round(min(min_ts)), round(max(max_ts))
    return range(min_ts,max_ts, int((max_ts-min_ts)/no_of_record))

def get_aggregated_record(start_time, end_time, data):
    temp = {}
    for i in range(len(data)):
        # print(len(data))
        temp_data = data[i].query(f'timestamp >= {start_time} & timestamp <= {end_time}')
        # print(len(temp_data))
        for j in temp_data.columns:
            temp[j] = temp_data[j].max()
            # print(temp[j])
        print(temp)
    return temp

def preprocess_data(mat, cols):
    ss = StandardScaler()
    # mat.sort_values("rpm", axis=0, inplace=True)
    mat = pd.DataFrame(ss.fit_transform(mat))
    mat.columns = cols
    del(ss)
    return mat

def fit(mat):
    #Prepare D Matrix 
    print("--------------- Calculating Similarity_operator(D,D') ---------------")
    retData = np.linalg.pinv(1.0 / (1 + (sbm.pairwise_distances(mat, metric = 'euclidean'))))
    return retData
    
def get_weights(training_mat, yin):
    temp_element_left = fit(training_mat)
    temp_element_right = (1.0 / (1.0 +(sbm.pairwise_distances(training_mat,yin,metric='euclidean'))))
    W_cap = np.matmul(temp_element_left, temp_element_right)
    sum_of_WCap = pd.DataFrame(W_cap).sum(axis=0)
    W_bar = W_cap / sum_of_WCap[0]
    return W_bar

def predict(mat, weights):
    print("PID SHAPE : " + str(mat.shape) + "wi Shape : " + str(weights.shape))
    yest = np.dot(mat.T, weights)
    #residue = abs(mat-yest.T)
    yest = pd.DataFrame(yest.T)
    yest.columns = cols
    return (yest)
    
def post_process(org, mat, cols):
    ss = StandardScaler()
    temp = ss.fit_transform(org)
    mat = pd.DataFrame(ss.inverse_transform(mat))
    mat.columns = cols
    del(ss)
    del(temp)
    return mat

def main():
    D = fetch(1639074601000,1639506601000,0, 'MAX')
    Yin = fetch(1639506601000,round(time.time())*1000,0, 'MAX')
    return D, Yin

if __name__ == '__main__':
    a = time.time()
    print(time.ctime(time.time()))
    # if(os.path.exists("Data.pkl")):
    #     agg_data = pickle.load("Data.pkl")
    # else:
    D, Yin = main()
    data = []
    for i in D[3]:
        data.append(pd.read_csv(i))
    for i in range(len(data)):
        data[i].drop(data[i].columns[0], axis = 1, inplace= True)
    read_data_by = list(get_min_max_ts(data, 1000))
    
    agg_data = [] #get_aggregated_record(st,et, data)
    for i in range(len(read_data_by)-1):
        agg_data.append(get_aggregated_record(read_data_by[i], read_data_by[i+1], data))
    agg_data = pd.DataFrame(agg_data)
    del(data)
    data = agg_data
    cols = data.columns
    data.drop(data.columns[0], axis = 1, inplace= True)
    data.dropna(how = 'all', inplace=True, axis = 0)
    #data.to_pickle("Data_Latest.pkl")
    
    ydata = []
    for i in Yin[3]:
        ydata.append(pd.read_csv(i))
    # for i in range(len(data)):
        # ydata[i].drop(ydata[i].columns[0], axis = 1, inplace= True)
    read_data_by = list(get_min_max_ts(ydata, 10))
    
    yagg_data = [] #get_aggregated_record(st,et, data)
    for i in range(len(read_data_by)-1):
        yagg_data.append(get_aggregated_record(read_data_by[i], read_data_by[i+1], ydata))
    yagg_data = pd.DataFrame(yagg_data)
    del(ydata)
    ydata = yagg_data
    cols = data.columns
    print("dropping --->",list(ydata.columns[0:2]))
    ydata.drop(list(ydata.columns[0:2]), axis = 1, inplace= True)
    # ydata.drop(["timestamp"], axis = 1, inplace= True)
    ydata.dropna(how = 'all', inplace=True, axis = 0)
    # preprocessed_data = preprocess_data(data, cols)
    # preprocessed_y_input = preprocess_data(ydata, cols)
    preprocessed_data = data
    preprocessed_y_input = ydata
    
    W = get_weights(preprocessed_data, preprocessed_y_input)
    predicted_values = predict(preprocessed_data, W)
    
    actual_data = post_process(data, preprocessed_data, cols)
    predicted_data = post_process(data, predicted_values, cols)   
    residue_values = predicted_data - ydata
    b = time.time()
    
    print(time.ctime(time.time()))
    print(b-a)    